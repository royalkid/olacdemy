from fastapi import FastAPI
from fastapi.responses import Response
import uvicorn
from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException as StarletteHTTPException
from routers import blog

tags_metadata = [
    {
        "name": "Blog",
        "description": "Endpoints related to operations on the **Blogs**\
            collection."
    }
]


app = FastAPI(
    title="Internship Task",
    description="Backend for the internship",
    version="1.0",
    openapi_tags=tags_metadata
)


@app.exception_handler(RequestValidationError) # Exception raised when wrong json format is sent
async def validation_exception_handler(exc):
    return Response(status_code=422, content=str(exc))


@app.exception_handler(StarletteHTTPException) #To return HTTP responses with errors to the client
async def http_exception_handler(exc):
    return Response(status_code=exc.status_code, content=exc.detail)



app.include_router(
    blog.router,
    prefix="/blog",
    tags=["Blog"]
)


if __name__ == "__main__":
    uvicorn.run(app)
