from typing import Optional
from pydantic import BaseModel


class Blog(BaseModel):
    title: Optional[str]
    media: Optional[str]
    content: Optional[str]
