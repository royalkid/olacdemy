from fastapi import APIRouter, HTTPException
from routers import db
from models import Blog

router = APIRouter()


@router.post("/add")
def add_blog(blog: Blog): #function to add blog details
    try:
        blog_ref = db.collection(u"blogs")
        blog_ref.add(dict(blog))
        return True
    except Exception as e:
        print(e)
        raise HTTPException(status=400, content=str(e))


@router.get("/{blog_id}")
def get_blog(blog_id):  #function to get blog details
    try:
        blog_ref = db.collection(u"blogs")
        blog_doc = blog_ref.document(blog_id).get()
        return blog_doc.to_dict()

    except Exception as e:
        print(e)
        raise HTTPException(status=400, content=str(e))


@router.delete("/{blog_id}")
def delete_blog(blog_id): #function to delete blog
    try:
        blog_ref = db.collection(u"blogs")
        blog_ref.document(blog_id).delete()
        return True

    except Exception as e:
        print(e)
        raise HTTPException(status=400, content=str(e))


@router.put("/{blog_id}")
def update_blog(blog_id, blog: Blog): #function to update blog details
    try:
        blog_ref = db.collection(u"blogs")
        blog_ref.document(blog_id).update(dict(blog))
        return True

    except Exception as e:
        print(e)
        raise HTTPException(status=400, content=str(e))
